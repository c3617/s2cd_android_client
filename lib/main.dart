import 'package:flutter/material.dart';
import 'dart:convert';

import 'package:http/http.dart' as http;

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool _recentValueTab = true;

  // String _apiUrl = 'http://192.168.29.124:5000/api';
  String _apiUrl = '';
  final apiTextController = TextEditingController();

  bool _splashScreen = true;
  bool _gotIp = false;

  String _value = '';
  String _dateAdded = '';
  String _error = '';

  Future<Map?> getRecentValue() async {
    try {
      Uri uri = Uri.parse('$_apiUrl/sensor_values');
      http.Response response = await http.get(uri);

      if (response.statusCode == 200) {
        Map jsonData = json.decode(response.body);
        List list = jsonData['data'];
        Map currentValue = list[list.length - 1];
        print(currentValue);
        _error = '';
        return currentValue;
      }
    } catch (e) {
      print(e);
      String err = e.toString();
      err = err.contains(':') ? err.substring(0, err.indexOf(':')) : err;
      setState(() => _error = err);
    }

    return null;
  }

  Future<void> triggerSensorRecord() async {
    try {
      Uri uri = Uri.parse('$_apiUrl/remote_trigger');
      http.Response response = await http.post(uri);
    } catch (e) {
      print(e);
      String err = e.toString();
      err = err.contains(':') ? err.substring(0, err.indexOf(':')) : err;
      setState(() => _error = err);
    }
  }

  Future<void> printRecentValue() async {
    Map? currentValue = await getRecentValue();
    if (currentValue != null) {
      setState(() {
        _value = currentValue['sensor value'].toString();
        _dateAdded = currentValue['date added'].toString();
      });
    }
  }

  Future<void> actionTriggered() async {
    await Future.delayed(const Duration(seconds: 5));
    await triggerSensorRecord();

    setState(() => _recentValueTab = true);
  }

  @override
  void initState() {
    super.initState();
    print('initState');

    // printRecentValue();
  }

  @override
  void dispose() {
    apiTextController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Flutter App'),
        ),
        body: Container(
          margin: const EdgeInsets.all(10),
          child: _splashScreen
              ? Center(
                  child: Wrap(
                  direction: Axis.vertical,
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: [
                    Image.asset(
                      'assets/flutter_icon.png',
                      width: 200,
                    ),
                    TextButton(
                      child: const Text("next"),
                      onPressed: () => setState(() => _splashScreen = false),
                    ),
                  ],
                ))
              : !_gotIp
                  ? Container(
                      padding: const EdgeInsets.all(10),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          TextField(
                              controller: apiTextController,
                              decoration: const InputDecoration(
                                border: OutlineInputBorder(),
                                labelText: 'API URL',
                              )),
                          ElevatedButton(
                              child: const Text('submit'),
                              onPressed: () {
                                if (apiTextController.text.isNotEmpty) {
                                  setState(() {
                                    _apiUrl = apiTextController.text;
                                    _gotIp = true;
                                  });
                                  printRecentValue();
                                }
                              })
                        ],
                      ),
                    )
                  : Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                          Expanded(
                            child: Container(
                              padding: const EdgeInsets.all(10),
                              // color: Colors.blue,
                              child: _recentValueTab
                                  ? Wrap(
                                      direction: Axis.vertical,
                                      alignment: WrapAlignment.center,
                                      crossAxisAlignment:
                                          WrapCrossAlignment.center,
                                      children: [
                                        const Text(
                                          'recent value tab',
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20,
                                          ),
                                        ),
                                        Text('current ip: $_apiUrl'),
                                        Text('sensor value: $_value'),
                                        Text('date added: $_dateAdded'),
                                        Text(
                                          _error,
                                          style: const TextStyle(
                                            color: Colors.red,
                                          ),
                                        ),
                                        Container(
                                          margin: const EdgeInsets.all(5),
                                          child: ElevatedButton(
                                            child: const Text('refresh'),
                                            onPressed: () => printRecentValue(),
                                          ),
                                        ),
                                      ],
                                    )
                                  : Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        const Text(
                                          'trigger sensor value record tab',
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20,
                                          ),
                                        ),
                                        Text(
                                          _error,
                                          style: const TextStyle(
                                            color: Colors.red,
                                          ),
                                        ),
                                        Container(
                                          margin: const EdgeInsets.all(5),
                                          child: ElevatedButton(
                                            child: const Text(
                                                'trigger sensor record'),
                                            onPressed: () => actionTriggered(),
                                          ),
                                        ),
                                      ],
                                    ),
                            ),
                          ),
                          Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  margin: const EdgeInsets.all(5),
                                  child: ElevatedButton(
                                    child: const Text('recent value'),
                                    onPressed: () =>
                                        setState(() => _recentValueTab = true),
                                  ),
                                ),
                                Container(
                                  margin: const EdgeInsets.all(5),
                                  child: ElevatedButton(
                                    child: const Text('trigger record value'),
                                    onPressed: () =>
                                        setState(() => _recentValueTab = false),
                                  ),
                                ),
                              ]),
                        ]),
        ),
      ),
    );
  }
}
